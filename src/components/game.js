import React, { Component } from "react";

import Board from "./board";

class Game extends Component {
  constructor(props) {
    super(props);

    this.state = {
      history: [{
        squares: new Array(9).fill(null),
        value: "",
        indexChanged: undefined
      }],
      stepNumber: 0,
      xIsNext: true
    }
  }

  handleClick(squareIndex) {
    if (this.state.stepNumber === this.state.history.length - 1) {
      const history = [...this.state.history];
      const current = history[history.length - 1];
      const squares = [...current.squares];
      if (!(calculateGameStatus(squares) || squares[squareIndex])) {
        const value = this.state.xIsNext ? "X" : "O";
        squares[squareIndex] = value;
        this.setState({
          history: this.state.history.concat({
            squares: squares,
            value: value,
            indexChanged: squareIndex
          }),
          stepNumber: history.length,
          xIsNext: !this.state.xIsNext
        });
      }
    }
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0
    });
  }

  calculateColumnNumber(indexChanged) {
    return Math.floor(indexChanged / 3) + 1;
  }
  calculateRowNumber(indexChanged) {
    return (indexChanged % 3) + 1;
  }
  getMoveHistoryLiElements() {
    return this.state.history.map((step, move) => {
      const desc = move ?
        `Go to move #${move}: ${step.value} on ${this.calculateColumnNumber(step.indexChanged)},${this.calculateRowNumber(step.indexChanged)}` :
        "Go to game start";

      return (
        <li key={move}>
          <button onClick={() => { this.jumpTo(move); }}>{desc}</button>
        </li>
      );
    });
  }

  render() {
    const current = this.state.history[this.state.stepNumber];
    const winner = calculateGameStatus(current.squares);
    const status = winner || `Next player: ${this.state.xIsNext ? "X" : "O"}`;
    const moves = this.getMoveHistoryLiElements();
    return (
      <div className="game" >
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={(i) => { this.handleClick(i); }}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

const lines = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

function isTie(squares) {
  return squares.length === 9 &&
    squares.every(box => box);
}

function getWinningCombo(squares) {
  return lines.find(victoryLines => {
    const [a, b, c] = victoryLines;
    return squares[a] && squares[a] === squares[b] && squares[a] === squares[c];
  });
}

function calculateGameStatus(squares) {
  const winningCombo = getWinningCombo(squares);
  if (winningCombo) {
    return `Winner: ${squares[winningCombo[0]]}`;
  } else if (isTie(squares)) {
    return "DRAW";
  } else {
    return undefined;
  }
}

export default Game;
