import React, { Component } from 'react';

import Game from "./components/game";
import "./style/app.css";

class App extends Component {
  render() {
    return (
      <Game />
    );
  }
}

export default App;
